package com.getaway.rps;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

public class Main {

	public static void main(String[] args) {
		Map<String, Integer> playerStats = new HashMap<String, Integer>();
		Map<String, Integer> choiceStats = new HashMap<String, Integer>();

		System.out.println("Trace of the iterations: ");
		IntStream.range(0, 100).forEach(i -> {
			RpsManager manager = new RpsManager();
			System.out.print("Interation #" + i + " ");
			manager.setChoices(RpsChoice.getRandomChoice());
			Player winner = manager.runGame();
			String name = "Tie";
			if (winner != null) {
				name = winner.getName();
				String choice =  winner.getChoice().name();
				choiceStats.put(choice, (choiceStats.get(choice) == null ? 0 : choiceStats.get(choice)) + 1);
			}
			playerStats.put(name, (playerStats.get(name) == null ? 0 : playerStats.get(name)) + 1);
		});
		
		printStats(playerStats, choiceStats);
	}

	/**
	 * Prints the stats about how many times each player won and what was the winning options
	 * @param playerStats
	 * @param choiceStats
	 */
	private static void printStats(Map<String, Integer> playerStats, Map<String, Integer> choiceStats) {
		System.out.println("");
		System.out.println("");
		System.out.println("=============Players Stats=================");
		playerStats.forEach((k, v) -> System.out.println((k + ":" + v)));
		System.out.println("");
		System.out.println("");
		System.out.println("=============Choices Stats=================");
		choiceStats.forEach((k, v) -> System.out.println((k + ":" + v)));
	}
}
