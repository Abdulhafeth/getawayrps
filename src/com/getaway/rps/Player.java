package com.getaway.rps;

public class Player {
	private RpsChoice choice;
	private String name;

	public Player(String name) {
		this.name = name;
	}

	public RpsChoice getChoice() {
		return choice;
	}

	public void setChoice(RpsChoice choice) {
		this.choice = choice;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name + " with choice: " + choice;
	}
}
