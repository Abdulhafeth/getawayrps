package com.getaway.rps;

import java.util.Random;

public enum RpsChoice {

	ROCK {
		@Override
		public boolean beats(RpsChoice other) {
			return other == SCISSOR;

		}
	},
	PAPER {
		@Override
		public boolean beats(RpsChoice other) {
			return other == ROCK;

		}
	},
	SCISSOR {
		@Override
		public boolean beats(RpsChoice other) {
			return other == PAPER;

		}
	};
	
	private static final RpsChoice[] VALUES = values();
	private static final int SIZE = VALUES.length;
	private static final Random RANDOM = new Random();

	/**
	 * A method that determines each choice can beat whom
	 * 
	 * @param other
	 *            the other choice
	 * @return true means I am stronger than the other
	 */
	public abstract boolean beats(RpsChoice other);



	/**
	 * Gets a random choice from the RPS choices
	 * @return RpsChoice
	 */
	public static RpsChoice getRandomChoice() {
		int x = RANDOM.nextInt(SIZE);
		return VALUES[x];
	}

}
