package com.getaway.rps;

public class RpsManager {

	// Player1 always chooses ROCK
	private Player player1 = new Player("First Player");

	// Player2 picks choices randomly
	private Player player2 = new Player("Second Player");

	public void setChoices(RpsChoice secondPlayerChoice) {
		// Player1 always chooses ROCK
		player1.setChoice(RpsChoice.ROCK);

		// Player2 picks choices randomly
		player2.setChoice(secondPlayerChoice);
	}

	/**
	 * Runs an RPS game between two players
	 * 
	 * @return winner player
	 */
	public Player runGame() {

		// run the logic of who is beating whom
		if (player1.getChoice() == player2.getChoice()) {
			// Okay, this is a tie
			System.out.println("It is a tie!");
			return null;
		} else if (player1.getChoice().beats(player2.getChoice())) {
			// Congratulations player1, you win
			System.out.println(player1.toString() + " won over " + player2.toString());
			return player1;
		} else {
			// congratulations player2, you win
			System.out.println(player2.toString() + " won over " + player1.toString());
			return player2;
		}
	}
}
