package test.com.getaway.rps;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.getaway.rps.Player;
import com.getaway.rps.RpsChoice;
import com.getaway.rps.RpsManager;

public class RpsManagerTest {

	@Test
	public void testRunGame() {
		RpsManager rpsManager = new RpsManager();
		rpsManager.setChoices(RpsChoice.ROCK);
		Player player = rpsManager.runGame();
		assertNull(player);
		
		rpsManager.setChoices(RpsChoice.PAPER);
		player = rpsManager.runGame();
		assertTrue(player.getName().equalsIgnoreCase("second player"));
		
		rpsManager.setChoices(RpsChoice.SCISSOR);
		player = rpsManager.runGame();
		assertTrue(player.getName().equalsIgnoreCase("first player"));
	}
}
