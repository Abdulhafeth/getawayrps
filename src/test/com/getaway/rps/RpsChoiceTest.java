package test.com.getaway.rps;

import static org.junit.Assert.*;

import org.junit.Test;

import com.getaway.rps.RpsChoice;

public class RpsChoiceTest {

	@Test
	public void testBeats() {
		RpsChoice rock = RpsChoice.ROCK;
		RpsChoice paper = RpsChoice.PAPER;
		RpsChoice scissor = RpsChoice.SCISSOR;

		assertTrue(paper.beats(rock));
		assertTrue(rock.beats(scissor));
		assertTrue(scissor.beats(paper));
	}
}
